import java.util.Scanner;

class ApplianceStore{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		
		Toaster[] toasterList = new Toaster[4];
		
		for (int i = 0; i < 4; i++){
			System.out.println("enter color");
			String color = keyboard.nextLine();
			System.out.println("enter price");
			int price = keyboard.nextInt();
			System.out.println("enter max temp");
			int maxTemp = keyboard.nextInt();
			keyboard.nextLine();
			toasterList[i] = new Toaster(color,price,maxTemp);

		}

		
		Toaster lastToaster = toasterList[3];
		System.out.println("enter new color for last toaster ");
		lastToaster.paintToaster(keyboard.nextLine());
		
		System.out.println("enter new max temp for last toaster ");
		lastToaster.setMaxTemp(keyboard.nextInt());
		
		System.out.println(" enter new price for last toaster");
		lastToaster.setPrice(keyboard.nextInt());
		//PRINTING NEW VALUES
		System.out.println(" new values for last toaster:");
		System.out.println(lastToaster.getColor());
		System.out.println(lastToaster.getMaxTemp());
		System.out.println(lastToaster.getPrice());


	}
}