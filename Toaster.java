public class Toaster{
	private String color;
	private int price;
	private int maxTemp;
	
	//Constructor
	public Toaster(String color, int price, int maxTemp){
		this.color = color;
		this.price = price;
		this.maxTemp = maxTemp;
	}
	
	public void toastBread(){
		System.out.println("your bread has reached " + maxTemp + " degrees celcius!");
	}
	
	public void discountPrice(){
		System.out.println("the price was " + this.price + " dollars, but the toaster doesnt work anymore so ");
		this.price = 0;
		System.out.println("now it is " + this.price + " dollars");
	}
	
	public void paintToaster(String color){
		if (canPaint(color) == true){
			this.color = color;
		}
	}
	
	private boolean canPaint(String color){
		if (color != "transparent"){
			return true;
		}
		else{
			return false;
		}
	}
	
	//get methods
	
	
	public String getColor(){
		return this.color;
	}
	
	
	public int getMaxTemp(){
		return this.maxTemp;
	}
	
	public int getPrice(){
		return this.price;
	}
	
	//set methods
	
	/*
	public void setColor(String color){
		this.color = color;
	}
	*/
	
	public void setMaxTemp(int maxTemp){
		this.maxTemp = maxTemp;
	}
	
	public void setPrice(int price){
		this.price = price;
	}
}